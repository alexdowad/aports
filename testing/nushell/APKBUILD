# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.43.0
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le" # limited by rust/cargo
license="MIT"
makedepends="cargo pkgconf openssl-dev libx11-dev libxcb-dev libgit2-dev oniguruma-dev"
for _sub in core extra; do
	subpackages="$subpackages $pkgname-plugins-$_sub:_plugins"
done
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz"

# use system oniguruma
export RUSTONIG_SYSTEM_LIBONIG=1

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen --features=extra
}

check() {
	cargo test --frozen --features=extra
}

package() {
	cargo install --frozen --offline --features=extra --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

_plugins() {
	local name=${subpkgname#"$pkgname-plugins-"}
	pkgdesc="Nushell $name plugins"
	depends="nushell"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/nu_plugin_${name}_* "$subpkgdir"/usr/bin/
}

sha512sums="6b929c35f02e80b8c1a9b64bd250663d41702629c3857b5cbef8e60a2b9682d9f9728b3afa6a65a632995e8835cbad86899219d28b170a04206b4ea29f51e0c2  nushell-0.43.0.tar.gz"