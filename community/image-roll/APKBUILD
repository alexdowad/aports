# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=image-roll
pkgver=1.4.1
pkgrel=0
pkgdesc="Simple and fast GTK image viewer with basic image manipulation tools"
url="https://github.com/weclaw1/image-roll"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="cargo gtk+3.0-dev"
source="https://github.com/weclaw1/image-roll/archive/$pkgver/image-roll-$pkgver.tar.gz"
options="!check"  # no tests provided

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

package() {
	local appid='com.github.weclaw1.ImageRoll'

	install -D -m755 target/release/image-roll -t "$pkgdir"/usr/bin/

	install -D -m644 src/resources/$appid.desktop -t "$pkgdir"/usr/share/applications/
	install -D -m644 src/resources/$appid.svg -t "$pkgdir"/usr/share/icons/hicolor/scalable/apps/
	install -D -m644 src/resources/$appid.metainfo.xml -t "$pkgdir"/usr/share/metainfo/
}

sha512sums="
4af7b849dc33f4fdbf088c8a604bcbd749976abf0be4b269e7edafacaf0dc7e4a5da0f91367e4d6687c206dedb13c6dbb293c30e642806da2cdb7e5d68f6a5c6  image-roll-1.4.1.tar.gz
"
